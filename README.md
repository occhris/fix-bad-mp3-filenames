# fix-bad-mp3-filenames

This is a script to fix filenames of mp3 files in music libraries that are not safe cross-platform, so that files
can safely be stored and synched between different filesystems.
It will fix references in a Rhythmbox library, if it finds one.

## System requirements ##

Currently our script is tested only on Linux (Ubuntu 19 to be specific). Contributions to expand support are welcome.

Dependencies:
* PHP

## Usage ##

Just execute through PHP, giving your music library path to do a dry-run:
```
php fix-bad-mp3-filenames.php ~/Music
```
If you don't provide a path, it will assume ~/Music.

To actually make changes:
```
php fix-bad-mp3-filenames.php ~/Music --live
```
