<?php

/**
 * fix-bad-mp3-filenames
 *
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

set_time_limit(0);
ini_set('memory_limit', '-1');
error_reporting(E_ALL);

$opts = getopt('', ['live', 'folder:']);
$live = isset($opts['live']);
$folder = $opts['folder'] ?? ($_SERVER['HOME'] . '/Music');

$ob = new MusicScanner();
$ob->process_dir($folder);

$errors = $ob->errors;
$project_file = $ob->project_file;
$renames = $ob->renames;
$rmdirs = $ob->rmdirs;

file_put_contents('log.dat', json_encode($ob)); // In case something goes wrong we want to know what was done

if ($project_file === null) {
    $_project_file = __DIR__ . '/rhythmdb.xml';
    if (is_file($_project_file)) {
        $project_file = $_project_file;
    }
}

if ($project_file === null) {
    $errors[] = 'Could not find Rhythmbox project file';
    $project_c = null;
} else {
    $project_c = file_get_contents($project_file);
}
$project_changed = false;

foreach ($renames as $rename) {
    $orig_path = $rename['orig_path'];
    $intermediate_path = $rename['intermediate_path'];
    $new_path = $rename['new_path'];

    echo "$intermediate_path -> $new_path\n";
    if ($live && !file_exists($new_path)) {
        @mkdir(dirname($new_path), 0777, true);
        rename($intermediate_path, $new_path);
    }

    if ($rename['is_mp3'] && $project_c !== null) {
        $prefix = '<location>file://';
        $suffix = '</location>';
        $from = $prefix . project_url_encode($orig_path) . $suffix;
        $to = $prefix . project_url_encode($new_path) . $suffix;
        if (strpos($project_c, $from) !== false) {
            $project_c = str_replace($from, $to, $project_c);
            $project_changed = true;
        } else {
            $errors[] = 'Cannot find ' . $orig_path . ' in project library';
        }
    }
}

if ($project_changed && $live) {
    file_put_contents($project_file, $project_c);
}

foreach (array_reverse($rmdirs) as $rmdir) {
    if ($live) {
        @rmdir($rmdir);
    }
}

foreach ($errors as $error) {
    echo "Error: $error\n";
}

class MusicScanner
{
    public $errors = [];
    public $renames = [];
    public $rmdirs = [];
    public $project_file = null;

    public function process_dir($new_folder, $orig_folder = null)
    {
        if ($orig_folder === null) {
            $orig_folder = $new_folder;
            $top = true;
        } else {
            $top = false;
        }

        $dh = opendir($orig_folder);
        if ($dh !== false) {
            while (($f = readdir($dh)) !== false) {
                if ($f == '.' || $f == '..') {
                    continue;
                }

                $orig_path = $orig_folder . '/' . $f;

                $ext = strtolower(pathinfo($f, PATHINFO_EXTENSION));
                $is_mp3 = ($ext == 'mp3');
                $is_dir = is_dir($orig_path);

                $new_f = $this->fix_name($f);

                $intermediate_path = $new_folder . '/' . $f;

                $new_path = $new_folder . '/' . $new_f;

                if (($new_f != $f) && (($is_dir) || ($is_mp3))) {
                    // Rename
                    if (file_exists($new_path)) {
                        if ($is_dir) {
                            $this->rmdirs[] = $orig_path;
                        } else {
                            $this->errors[] = "The fixed path, $new_path, already exists.";
                        }
                    } else {
                        $this->renames[] = [
                            'orig_path' => $orig_path,
                            'intermediate_path' => $intermediate_path,
                            'new_path' => $new_path,
                            'is_mp3' => $is_mp3,
                        ];
                    }
                } elseif ($f == 'rhythmdb.xml') {
                    // Project file
                    if ($this->project_file !== null) {
                        $this->errors[] = 'Found multiple rhythmdb.xml files';
                    } else {
                        $this->project_file = $orig_path;
                    }
                }

                if ($is_dir) {
                    // Recurse
                    $this->process_dir($new_path, $orig_path);
                }
            }

            closedir($dh);
        }
    }

    protected function fix_name($f)
    {
        $map = [
            '<' => '(',
            '>' => ')',
            ':' => '--',
            '"' => "'",
            '\\' => '--',
            '|' => '-',
            '?' => '',
            '*' => '',
        ];

        $new_f = str_replace(array_keys($map), array_values($map), $f);

        $new_f = transliterator_transliterate('Any-Latin; Latin-ASCII', $new_f);

        return $new_f;
    }
}

function project_url_encode($in)
{
    $map = [
        '%2F' => '/',
        '%28' => '(',
        '%29' => ')',
        '%2C' => ',',
        '%26' => '&',
        '%27' => "'",
        '%3A' => ':',
        '%40' => '@',
        '%21' => '!',
        '%2A' => '*',
        '%2B' => '+',
    ];
    return htmlentities(str_replace(array_keys($map), array_values($map), rawurlencode($in)), ENT_XML1);
}
